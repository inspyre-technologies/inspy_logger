#InSPY Logger Overview

----

### What is it?

InSPy Logger is a library package made for Python3, and is installable in any environment that has 
access to PIP for Python3 and an internet connection.

----

### Target End-User?

Honestly, myself, and maybe some people I write code with. Not that the whole world isn't welcome to use it, just please
understand that as the project currently stands I do not make any claims as to how useful you might find this library. 
I *do* know that - as of now - this library only exists to support a couple applications I've written

----

### Is It Safe to Use as a Dependency?

As far as I know I do not plan on pulling any published releases down from any sources (barring any unforeseen security
reasons) except possibly pre-release versions. As I said in the section above, everyone is free to use it. Please see
 the appropriate documentation if you do plan on using inSPy Logger for your own application!
 
 **Pro-Tip**:
 *You should lock the version of inspy_logger you're using if you're using it in a production application or library
  as I make* **no** *guarantees about the library's API staying consistent between version releases.*

----
  
### Can I Contribute?

Absolutely, though I advise you read the CODE_OF_CONDUCT before doing so.