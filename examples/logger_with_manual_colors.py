from inspy_logger import start as start_log
from inspyred_print import Color, Effects, Format

info = None


def start_logger():
    """
    
    :return:
    :rtype:
    """
    global info
    _log = start_log('ExampleApplication', True)
    info = _log.info


def main():
    """
    
    :return:
    :rtype:
    """
    global info
    green = Color.green
    red = Color.red
    fx = Effects
    end = Format.end_mod
    print('Here is a regular string with no color, effects, or formatting')
    print(f'{fx.blink}{green}Here is a green blinking string.{end}')
    start_logger()
    info(f'{red}I am an info log with red text!{end}')
    info('I am a regular info log')


main()